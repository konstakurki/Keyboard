module switch(a,b) {
    translate([a,b,0]) cube([14,14,30],center=true);
    translate([a,b,-101.5]) cube([17.5,17.5,200],center=true);
}

module frameblock(a,b) {
    difference() {
        translate([a,b,-3]) cube([19,19,6],center=true);
        switch(a,b);
    }
}

module screwplate(a,b,theta) {
    translate([a,b,-6]) rotate([0,0,theta]) difference() {
        cube([9,9,6]);
        translate([4.5,4.5,0]) cylinder(h=80,r=1.5,center=true,$fn=15);
    }
}

module finger(a,b) {
    for(x = [-19:19:20]) frameblock(a,x+b);
}

module thumb() {
    frameblock(19,-38);
    frameblock(0,-38);
    frameblock(-19,-38);
    frameblock(0,-57);
    frameblock(-19,-57);
    frameblock(-38,-57);
    screwplate(-28.5,-47.5,90);
    screwplate(9.5,-47.5,-90);
    screwplate(-14,-66.5,-90);
}

module fingers() {
    finger(-19,0);
    finger(0,0);
    finger(19,5);
    finger(38,2);
    finger(57,-4);
    finger(76,-4);
    screwplate(-28.5,-9.5,180);
    screwplate(-28.5,9.5,90);
    screwplate(28.5,30.5,0);
    screwplate(47.5,-26.5,180);
    screwplate(85.5,4.5,0);
    screwplate(85.5,-13.5,-90);
    translate([-9.5,9.5,-3]) cylinder(h=6,r=2,center=true,$fn=15);
}

difference() {
    fingers();
    translate([-9.5,9.5,0]) cylinder(h=80,r=1.5,center=true,$fn=15);
}

translate([0,-5,0]) thumb();

