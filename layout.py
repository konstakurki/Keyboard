# -*- coding: utf-8 -*-

import sys

alphabet = 'abcdefghijklmnopqrstuvwxyzåäö'

def english_ngrams(n):
    f = open('ngrams'+str(n)+'.csv')
    items = f.readlines()
    f.close()
    items = items[1:]
    dict = {}
    if n == 1:
        dict['ä'] = 0.0
        dict['ö'] = 0.0
        dict['å'] = 0.0
    for i in items:
        dict[i[0:n].lower()] = float(int(i[n+1:i.find(',',n+1)]))
    m = 0
    for i in dict:
        m = m + dict[i]
    for i in dict:
        dict[i] = 100*dict[i]/m

    return dict

def lefthand(layout):
    s = ''
    for i in [0,1,2]:
        s = s + layout[i][0:6]
    return s

def righthand(layout):
    s = ''
    for i in [0,1,2]:
        s = s + layout[i][6:-1]
    return s

def homekeys(layout):
    s = layout[1][1:5]+layout[1][7:11]
    return s

def difficultkeys(layout):
    s = ''
    for i in [0,2]:
        for n in [0,5,6,11]:
            s = s + layout[i][n]
    return s

def keysunderfinger(l,finger):
    s = ''
    for i in [0,1,2]:
        for n in finger:
            s = s + l[i][n]
    return s

def same_hand_digrams(layout,digrams,thres):
    shd = []
    shd_imp = []
    p = 0
    lh = lefthand(layout)
    rh = righthand(layout)
    for i in digrams:
        if (i[0] in lh) != (i[1] in rh):
            if i[0] != i[1]:
                shd.append(i)
                p_n = digrams[i]
                p = p + p_n
                if p_n > thres:
                    shd_imp.append(i)
    return [shd,p,shd_imp]

def finger(s,lo):
    dic = {0:0,1:0,2:1,3:2,4:3,5:3,6:4,7:4,8:5,9:6,10:7,11:7}
    n = lo[0].find(s)
    if n == -1:
        n = lo[1].find(s)
    if n == -1:
        n = lo[2].find(s)
    n = dic[n]
    return n


def same_finger_digrams(layout,digrams,thres):
    shd = []
    shd_imp = []
    p = 0
    lh = lefthand(layout)
    rh = righthand(layout)
    for i in digrams:
        if finger(i[0],layout) == finger(i[1],layout):
            if i[0] != i[1]:
                shd.append(i)
                p_n = digrams[i]
                p = p + p_n
                if p_n > thres:
                    shd_imp.append(i)
    return [shd,p,shd_imp]

def share(string,lan):
    p = 0
    for i in string:
        if i in alphabet:
            p = p + lan[i]
    return p

def properties(lo,lan,dig):
    s = lefthand(lo)
    s = s.replace(' ','')
    line = 'Left hand ('+s+'): '+str(share(s,lan))
    print(line)

    s = righthand(lo)
    s = s.replace(' ','')
    line = 'Right hand ('+s+'): '+str(share(s,lan))
    print(line)

    s = homekeys(lo)
    s = s.replace(' ','')
    line = 'Home keys ('+s+'): '+str(share(s,lan))
    print(line)

    s = difficultkeys(lo)
    s = s.replace(' ','')
    line = 'Difficult keys ('+s+'): '+str(share(s,lan))
    print(line)

    print('')

    s = keysunderfinger(lo,[0,1])
    s = s.replace(' ','')
    line = 'Left pinky finger ('+s+'): '+str(share(s,lan))
    print(line)

    s = keysunderfinger(lo,[2])
    s = s.replace(' ','')
    line = 'Left ring finger ('+s+'): '+str(share(s,lan))
    print(line)

    s = keysunderfinger(lo,[3])
    s = s.replace(' ','')
    line = 'Left middle finger ('+s+'): '+str(share(s,lan))
    print(line)

    s = keysunderfinger(lo,[4,5])
    s = s.replace(' ','')
    line = 'Left index finger ('+s+'): '+str(share(s,lan))
    print(line)

    print('')

    s = keysunderfinger(lo,[6,7])
    s = s.replace(' ','')
    line = 'Right index finger ('+s+'): '+str(share(s,lan))
    print(line)

    s = keysunderfinger(lo,[8])
    s = s.replace(' ','')
    line = 'Right middle finger ('+s+'): '+str(share(s,lan))
    print(line)

    s = keysunderfinger(lo,[9])
    s = s.replace(' ','')
    line = 'Right ring finger ('+s+'): '+str(share(s,lan))
    print(line)

    s = keysunderfinger(lo,[10,11])
    s = s.replace(' ','')
    line = 'Right pinky finger ('+s+'): '+str(share(s,lan))
    print(line)

    print('')

    di = same_hand_digrams(lo,dig,0.3)
    digs = ''
    for i in di[2]:
        digs = digs + ', ' + i
    digs = digs[2:]
    print('The most significant same hand digrams: '+digs)
    print()
    print(str(di[1])+'% of all non-double digrams are same hand digrams.')

    print('')

    di = same_finger_digrams(lo,dig,0.08)
    digs = ''
    for i in di[2]:
        digs = digs + ', ' + i
    digs = digs[2:]
    print(str(di[1])+'% of all non-double digrams are same finger digrams.')
    print()
    print('The most significant same finger digrams: '+digs)

def main():
    filename = sys.argv[1]
    f = open(filename)
    leiska = f.readlines()
    f.close
    print('Layout:')
    for i in leiska:
        print(i[0:-1])
    #print('\nNumbers for English:')
    print()
    properties(leiska,english_ngrams(1),english_ngrams(2))
    #print('\nNumbers for Finnish:')
    #properties(leiska,suomi)
    #same_hand_digrams(leiska,english_digrams)
    #print(english_ngrams(1))

if __name__ == '__main__':
    main()
