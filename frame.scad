module switch(r,theta,gamma,delta)
{
    rotate([gamma,theta,delta])
    union() {
        color("red") translate([0,0,-r-14])
            union() {
                translate([0,0,-0.75])
                    cube([14,14,2.5],center = true);
                translate([0,0,-5.65])
                    cube([17,17,8.3],center = true);
                translate([0,0,2.5])
                    cube([19,19,5],center = true);
                translate([0,0,9])
                    cube([15,15,10],center = true);
                }
        /*color("green") cube([3,3,3], center = true);*/
    }

}

    module frameblock(r,theta,gamma,delta,a,b,c,oa,ob,oc)
{
    rotate([gamma,theta,delta])
        translate([0,0,-r-14])
            translate([oa,ob,oc-0.5*c]) cube([a,b,c],center = true);

}

module pillar(x,y,h) {
    translate([x,y,0.5*h]) cube([5,5,h],center=true);
}

module indexswitches()
{
    theta = 20;
    r = 45;
    union() {
        switch(r,-1.5*theta,0,0);
        switch(r,-0.5*theta,0,0);
        switch(r,0.5*theta,0,0);
        switch(r,1.5*theta,0,0);
    }
}

module indexextraswitches()
{
    theta = 20;
    r = 45;
    translate([0,7,2]) union() {
        switch(r,-21,13,0);
        switch(r,-0,13,0);
        switch(r,21,13,0);
        }
}

module indexframe()
{
    theta = 20;
    r = 45;
    w = 22.2;
    d = 19;
    t = 4;
    translate([0,0,0]) union() {
        frameblock(r,-1.5*theta,0,0,w,d,t,0,0,0);
        frameblock(r,-0.5*theta,0,0,w,d,t,0,0,0);
        frameblock(r,0.5*theta,0,0,w,d,t,0,0,0);
        frameblock(r,1.5*theta,0,0,w,d,t,0,0,0);
        }

}

module indexextraframe()
{
    theta = 20;
    r = 45;
    w = 23.2;
    d = 19;
    t = 4;
    translate([0,7,2]) union() {
        frameblock(r,-21,13,0,w,22,t,0,-1.5,0);
        frameblock(r,-0,13,0,w,22,t,0,-1.5,0);
        frameblock(r,21,13,0,w,22,t,0,-1.5,0);
    }
}
module middleswitches()
{
    theta = 18;
    translate([5,-19,2]) union() {
        switch(50,-1.5*theta,0,0);
        switch(50,-0.5*theta,0,0);
        switch(50,0.5*theta,0,0);
        switch(50,1.5*theta,0,0);
    }
}

module middleframe()
{
    theta = 18;
    r = 52;
    w = 21.7;
    d = 19;
    t = 4;
    translate([5,-19,2]) union() {
        frameblock(r,-1.5*theta,0,0,w,d,t);
        frameblock(r,-0.5*theta,0,0,w,d,t);
        frameblock(r,0.5*theta,0,0,w,d,t);
        frameblock(r,1.5*theta,0,0,w,d,t);
    }
}

module ringswitches()
{
    theta = 18;
    translate([1,-38,2]) union() {
        switch(50,-1.5*theta,0,0);
        switch(50,-0.5*theta,0,0);
        switch(50,0.5*theta,0,0);
        switch(50,1.5*theta,0,0);
    }
}

module ringframe()
{
    theta = 18;
    r = 52;
    w = 21.8;
    d = 19;
    t = 4;
    translate([1,-38,2]) union() {
        frameblock(r,-1.5*theta,0,0,w,d,t);
        frameblock(r,-0.5*theta,0,0,w,d,t);
        frameblock(r,0.5*theta,0,0,w,d,t);
        frameblock(r,1.5*theta,0,0,w,d,t);
    }
}

module pinkyswitches()
{
    theta = 22;
    translate([-6,-57,-2]) union() {
        switch(40,-1.5*theta,0,0);
        switch(40,-0.5*theta,0,0);
        switch(40,0.5*theta,0,0);
        switch(40,1.5*theta,0,0);

        translate([0,-7,2]) union() {
        switch(40,-23,-13,0);
        switch(40,-0,-13,0);
        switch(40,23,-13,0);
        }
    }
}

module pinkyframe()
{
    theta = 22;
    r = 40;
    w = 22.7;
    d = 19;
    t = 4;
    union() {
        translate([-6,-57,-2]) union() {
            frameblock(r,-1.5*theta,0,0,w,d,t,0,0,0);
            frameblock(r,-0.5*theta,0,0,w,d,t,0,0,0);
            frameblock(r,0.5*theta,0,0,w,d,t,0,0,0);
            frameblock(r,1.5*theta,0,0,w,d,t,0,0,0);
        }
    }
}

module pinkyextraframe()
{
    theta = 22;
    r = 40;
    w2 = 22.3;
    d2 = 19;
    t2 = 4;
    translate([-6,-57,-2]) translate([0,-7,2]) union() {
        frameblock(r,-21,-13,0,w2,21,t2,0,1,0);
        frameblock(r,-0,-13,0,w2,21,t2,0,1,0);
        frameblock(r,21,-13,0,w2,21,t2,0,1,0);
    }
}

module thumbswitches()
{
    a = -90;
    b = 20;
    r = 60;
    d = 15;
    translate([-85,19,-31]) union() {
        /*cube([3,3,3],center = true);*/
        translate([43,-19,0]) switch(0,24,0,0);
        translate([48,0,0]) switch(0,9,0,0);
        translate([45,19,0]) switch(0,4,0,0);
        translate([34,38,0]) switch(0,0,0,0);
    }
}

module thumbframe()
{
    a = -90;
    b = 20;
    r = 60;
    d = 15;
    w2 = 21;
    d2 = 21;
    t2 = 4;
    translate([-85,19,-29]) union() {
        translate([43,-19,0]) frameblock(2,24,0,0,w2,d2,t2,0,0,0);
        translate([48,0,0]) frameblock(2,9,0,0,w2,d2,5,0,0,-0.5);
        translate([45,19,0]) frameblock(2,4,0,0,w2,d2,t2,0,0,0);
        translate([34,38,0]) frameblock(2,0,0,0,w2,d2,t2,0,0,0);
    }
}

module switches()
{
    translate([0,0,85]) rotate([15,0,0]) union() {
        indexextraswitches();
        indexswitches();
        middleswitches();
        ringswitches();
        pinkyswitches();
        pinkyextraswitches();
        thumbswitches();
    }
}

module frame()
{
    translate([0,0,85]) rotate([15,0,0]) union() {
        indexextraframe();
        indexframe();
        middleframe();
        ringframe();
        pinkyframe();
        pinkyextraframe();
        thumbframe();
    }
    pillars();
}

module pillars() {
    pillar(16,-49,17);
    pillar(-18,-51,14);

    pillar(36,-34,31);

    pillar(43,6,38);
    pillar(-53,1,42);

    pillar(-53,57,54);
    pillar(-11,34,36);
}

module keyboardframe() {
    difference() {
        frame();
        switches();
    }
}

keyboardframe();
/*mirror() rotate([0,0,180]) keyboard();*/

/*translate([0,0,-1]) cube ([200,200,1],center = true);/*

